CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Bryan Sharpe <bryansharpe@gmail.com>

Field Stripe allows the use of Stripe Checkout with just a regular decimal field.

INSTALLATION
------------

 * Copy the folder into your modules directory
 * Install the module.
 * Install the Stripe PHP library to sites/all/libraries/stripe-php. Alternatively, you can install it with drush: drush field-stripe-download.
 * Add your Stripe API keys and settings at /admin/config/services/field-stripe
 * Change any decimal field to 'Stripe Checkout' under 'Manage Display' of your content-type (bundle) you added the field to
 * Start taking super easy payments
 * Rejoice


MAINTENANCE
-------------

This project is not actively developed, but I will try to help
with any bugs/issues/etc.
