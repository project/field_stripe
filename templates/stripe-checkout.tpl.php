<?php
/**
 * @file stripe-checkout.tpl.php
 *
 * Handles outputting a stripe payment script
 *
 * @param $stripe array
 *  An array of setting used to output the form
 * @param $entity_type string
 *  The entity type
 * @param $entity_id int
 *  The entity id
 * @param $target string
 *  The machine name of the field containing the amount to charge
 */
$redirect = urlencode($stripe['redirect']);
$fragment = $stripe['fragment'];
unset($stripe['redirect']);
unset($stripe['fragment']);

$redirect_query = array('query' => array('redirect' => $redirect));
if (!empty($fragment)) {
  $redirect_query['query']['fragment'] = $fragment;
}

?>
<form class="field-stripe-form stripe-checkout-<?php print $entity_id; ?>" action="<?php print url('field-stripe/charge/', $redirect_query); ?>" method="POST">
  <noscript>You must <a href="http://www.enable-javascript.com" target="_blank">enable JavaScript</a> in your web browser in order to pay.</noscript>
  <input type="hidden" name="type" value="<?php print $entity_type; ?>">
  <input type="hidden" name="id" value="<?php print $entity_id; ?>">
  <input type="hidden" name="target" value="<?php print $target; ?>">

  <input
    type="submit"
    value="<?php print("Pay with card"); ?>"
    <?php foreach ($stripe as $key => $value): ?>
      <?php print 'data-' . $key . '="' . $value . '" '; ?>
    <?php endforeach; ?>
  />
</form>
