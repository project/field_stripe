<?php

/**
 * Settings form.
 */
function field_stripe_settings_form() {
  // Load up settings
  $stripe_settings = variable_get('field_stripe_settings', array());

  $form['field_stripe_settings'] = array(
    '#tree' => TRUE,
  );

  // Easier to read
  $stripe_form = &$form['field_stripe_settings'];

  // Admin
  $admin = $stripe_settings['admin'];

  $stripe_form['admin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Admin Settings'),
  );

  $stripe_form['admin']['create_customer'] = array(
    '#title' => t('Create Stripe Customers for authenticated users'),
    '#type' => 'checkbox',
    '#default_value' => isset($admin['create_customer']) ? $admin['create_customer'] : TRUE,
  );

  // Key Settings
  $keys = $stripe_settings['keys'];

  $stripe_form['keys'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stripe API Keys'),
    '#description' => t('The test or live keys from <a href="!url">your Stripe dashboard</a>.', array(
      '!url' => 'https://dashboard.stripe.com/account/apikeys',
    )),
  );
  $stripe_form['keys']['secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#default_value' => isset($keys['secret_key']) ? $keys['secret_key'] : '',
  );
  $stripe_form['keys']['publishable_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Publishable Key'),
    '#default_value' => isset($keys['publishable_key']) ? $keys['publishable_key'] : '',
  );

  $stripe_form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Stripe Settings'),
  );

  // Setting Vars
  $settings = $stripe_settings['settings'];

  $stripe_form['settings']['image'] = array(
    '#title' => t('Checkout Icon'),
    '#type' => 'managed_file',
    '#default_value' => isset($settings['image']) ? $settings['image'] : 0,
    '#upload_validators' => array(
      'file_validate_extensions' => array('png jpg jpeg'),
    ),
    '#upload_location' => 'public://stripe_image'
  );

  // @TODO: update to supported currency select list
  $stripe_form['settings']['currency'] = array(
    '#title' => t('Checkout Currency'),
    '#type' => 'textfield',
    '#default_value' => isset($settings['currency']) ? $settings['currency'] : 'CAD',
  );

  $stripe_form['extra']['success_message'] = array(
    '#title' => t('Success Message'),
    '#type' => 'textfield',
    '#description' => t('Leave blank for no message'),
    '#default_value' => isset($settings['success_message']) ? $settings['success_message'] : '',
  );

  $form['#submit'][] = '_field_stripe_image_upload';

  return system_settings_form($form);
}

/**
 * Make the stripe logo permanent
 *
 * @param $form
 * @param $form_state
 */
function _field_stripe_image_upload($form, &$form_state) {
  if (!empty($form_state['values']['field_stripe_settings']['settings']['image'])) {
    $image_file = file_load($form_state['values']['field_stripe_settings']['settings']['image']);
    $image_file->status = FILE_STATUS_PERMANENT;
    file_save($image_file);
    file_usage_add($image_file, 'field_stripe', 'user', 1);
  }
}
