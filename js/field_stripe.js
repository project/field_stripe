(function ($) {
  Drupal.behaviors.fieldStripe = {
    attach: function (context, settings) {

      var stripeForms = $(".field-stripe-form", context);
      if (stripeForms.length) {
        stripeForms.each(function () {
          var $form = $(this);

          $(':submit', $form).on('click', function (event) {
            event.preventDefault();
            var $button = $(this);
            var opts = $.extend({}, $button.data(), {
              token: function (result) {
                $form.append($('<input>').attr({type: 'hidden', name: 'stripeToken', value: result.id})).submit();
              }
            });
            StripeCheckout.open(opts);
          });
        });
      }
    }
  };
})(jQuery);
